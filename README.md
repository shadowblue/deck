# shadowblue image for Steam Deck &nbsp; [![pipeline status](https://gitlab.com/shadowblue/deck/badges/main/pipeline.svg)](https://gitlab.com/shadowblue/deck/-/commits/main) &nbsp; [![Quay Images](https://img.shields.io/badge/Quay-Images-green)](https://quay.io/shadowblue/deck)

An image for Steam Deck built on top of [bazzite gnome image](https://github.com/ublue-os/bazzite) with some tweaks applied and some bloat removed.

Changes in this image in comparison with [bazzite gnome image](https://github.com/ublue-os/bazzite):

- Increased zram size to ram size
- Additional gnome-shell extensions installed (see [recipes](./recipes))
- Changed some GNOME defaults (see [gschema overrides](./files/gschema-overrides/) and [dconf defaults](./files/system/etc/dconf/db/local.d/))
- Added [AllTheTools repo](https://gitlab.com/shadowblue/allthetools)
- Added script to easily enable Betterfox user.js and Firefox GNOME theme:
  - Run `tweak_firefox` in your terminal to enable them
  - See `tweak_firefox -h` for non-interactive options
  - You can place your user.js overrides in `user-overrides.js` file inside your profile directory
  - Or you can place such overrides in `files/system/usr/share/shadowblue/userjs/overrides/` directory in your repo for image-wide overrides

P.S. You can see reasons for most of this changes in [recipes directory](./recipes).

## Installation

> [!WARNING]
> [This is an experimental feature](https://www.fedoraproject.org/wiki/Changes/OstreeNativeContainerStable), try at your own discretion.

To rebase an existing atomic Fedora installation to the latest deck image (see [available tags](#available-tags) for other options):

- **Recommended:** reset all modifications of immutable image (layered packages, overrides, etc):
  ```
  rpm-ostree reset
  ```
- Rebase to the unsigned image, to get the proper signing keys and policies installed:
  ```
  rpm-ostree rebase ostree-unverified-registry:quay.io/shadowblue/deck:latest
  ```
- Reboot to complete the rebase:
  ```
  systemctl reboot
  ```
- Note that on the first boot after rebase, the system will remove all flatpak applications installed from the Fedora repo and install applications from Flathub. After installation, there is a chance that the icons of these applications will be missing. Don't panic, after the next rebase+reboot this should be fixed. If not, try switching the dark/light theme. If nothing helps, open the issue in this repo.
- Then rebase to the signed image, like so:
  ```
  rpm-ostree rebase ostree-image-signed:docker://quay.io/shadowblue/deck:latest
  ```
- Reboot again to complete the installation
  ```
  systemctl reboot
  ```

## Schedule

These images start building every day at 04:00 UTC. Usually the images are ready at 04:10 UTC.

## Variants

### Supported versions

- Latest Fedora release

### Available tags

- :latest (tracks newest fedora version released)
- :${fedora_version} (e.g. :41)

Each tag has it's timestamp version for easier rollback and release pinning. E.g. :41 → :41-20241030, :latest → :latest-20241030, etc.

### Tags expiration policy

Tags for latest version expire after 365 days.

## ISO

If build on Fedora Atomic, you can generate an offline ISO with the instructions available [here](https://blue-build.org/learn/universal-blue/#fresh-install-from-an-iso). These ISOs cannot unfortunately be distributed on GitLab for free due to large sizes, so for public projects something else has to be used for hosting.

## Verification

These images are signed with [Sigstore](https://www.sigstore.dev/)'s [cosign](https://github.com/sigstore/cosign). You can verify the signature by downloading the `cosign.pub` file from this repo and running the following command:

```bash
cosign verify --key cosign.pub quay.io/shadowblue/deck:latest
```

## Contact us

- [Matrix Space](https://matrix.to/#/#shadowblue:matrix.org) (`#shadowblue:matrix.org`)
- [Telegram Chat](https://t.me/shadowblue_linux) (`@shadowblue_linux`)

## License

All license files can be found in the top directory of the source code or in /usr/share/licenses/shadowblue in the built images.

```
Copyright 2024 BlueBuild developers <bluebuild@xyny.anonaddy.com>
Copyright 2024-2025 Andrey Brusnik <dev@shdwchn.io>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

Portions of this software are copyright of their respective authors and were originally released under the MIT license:

- Files `user.js` and `user_hardened.js` inside `files/system/usr/share/shadowblue/` directory: Betterfox, Copyright © 2020 yokoffing. For licensing see LICENSE.Betterfox file
- File `files/system/usr/libexec/shadowblue/generate_userjs.sh`: Minimal safe Bash script template, Copyright © 2020 Maciej Radzikowski. For licensing see LICENSE.BashScriptTemplate file
