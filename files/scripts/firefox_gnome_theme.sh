#!/usr/bin/env bash
# SPDX-FileCopyrightText: Copyright © 2024-2025 Andrey Brusnik <dev@shdwchn.io>
#
# SPDX-License-Identifier: Apache-2.0

set -Eeuo pipefail

version="135"

curl -L "https://github.com/rafaelmardojai/firefox-gnome-theme/archive/refs/tags/v${version}.tar.gz" -o firefox-gnome-theme.tar.gz
tar xf firefox-gnome-theme.tar.gz
mv "firefox-gnome-theme-${version}" /usr/share/firefox-gnome-theme
rm firefox-gnome-theme.tar.gz
