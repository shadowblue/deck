#!/usr/bin/env bash
# SPDX-FileCopyrightText: Copyright © 2024-2025 Andrey Brusnik <dev@shdwchn.io>
#
# SPDX-License-Identifier: Apache-2.0
#
# Portions of this file are copyright of their respective authors and were originally released under the MIT license:
# - Minimal safe Bash script template, Copyright © 2020 Maciej Radzikowski. For licensing see LICENSE.BashScriptTemplate file
#
# All license files can be found in the top directory of the source code or in /usr/share/licenses/shadowblue in the built images.

set -Eeuo pipefail

usage() {
  cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [-h] [-v] [--hardened] [-p path/to/profile]
Generate user.js file for Firefox with shadowblue defaults.
Available options:
-h, --help        Print this help and exit
-v, --verbose     Print script debug info
    --hardened    Generate hardened variant
-p, --profile     Print generated output to the user.js file inside profile directory and use user-overrides.js from it
EOF
  exit
}

# shellcheck disable=SC2034
setup_colors() {
  if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}

msg() {
  echo >&2 -e "${1-}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "${RED}${msg}${NOFORMAT}"
  exit "$code"
}

parse_params() {
  # default values of variables set from params
  hardened=0
  profile=''

  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -v | --verbose) set -x ;;
    --no-color) NO_COLOR=1 ;;
    --hardened) hardened=1 ;;
    -p | --profile)
      profile="${2-}"
      shift
      ;;
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
    shift
  done

  return 0
}

apply_overrides() {
  local template=$1
  local overrides_path=$2

  for override_file in $overrides_path; do
    changed_prefs=$(grep -o '("[^("]*"' "$override_file" | sed 's/(//g' | tr '\n' '|' | sed -e 's/|$//' -e 's/|/\\|/g')
    # Comment changed preferences in template and apply overrides
    template=$(echo "$template" | sed "/$changed_prefs/s/^/\/\/ /" && cat "$override_file")
  done

  echo "$template"
}

parse_params "$@"
setup_colors

if [[ hardened -eq 1 ]]; then
  result=$(apply_overrides "$(cat /usr/share/betterfox/user.js)" /usr/share/shadowblue/userjs/default/*)
  result=$(apply_overrides "$result" /usr/share/shadowblue/userjs/hardened/*)
else
  result=$(apply_overrides "$(cat /usr/share/betterfox/user.js)" /usr/share/shadowblue/userjs/default/*)
fi

# Apply user's image-wide overrides
if [[ -d "/usr/share/shadowblue/userjs/overrides" ]]; then
  result=$(apply_overrides "$result" /usr/share/shadowblue/userjs/overrides/*)
fi

if [[ -n "${profile}" ]]; then
  # Apply profile local overrides
  if [[ -f "${profile}/user-overrides.js" ]]; then
    result=$(apply_overrides "$result" "${profile}/user-overrides.js")
  fi

  echo "$result" > "${profile}/user.js"
else
  echo "$result"
fi
